CREATE TABLE concursuri(
	id_conc INT auto_increment,
    denumire VARCHAR(255) NOT NULL,
    localitate VARCHAR(255) NOT NULL,
    data DATE,
    PRIMARY KEY(id_conc)
);