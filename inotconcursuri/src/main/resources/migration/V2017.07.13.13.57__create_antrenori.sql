CREATE TABLE antrenori(
	id_ant INT auto_increment,
    nume_ant VARCHAR(100) NOT NULL,
    prenume_ant VARCHAR(255) NOT NULL,
    nastere DATE,
    PRIMARY KEY(id_ant)
);