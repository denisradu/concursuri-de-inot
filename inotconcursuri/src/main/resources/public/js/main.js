/*var gameContainer = document.getElementById("game-info");
var btn = document.getElementById("btn");

btn.addEventListener("click",function(){

    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', 'json/antrenori.json');
    ourRequest.onload = function() {
        var ourData = JSON.parse(ourRequest.responseText);
        renderHTML(ourData);
        };
    ourRequest.send();

});

function renderHTML(data){
    var htmlString = "";

    for(i=0;i<data.length;i++){
        htmlString += "<p>" + data[i].name + " -- " + data[i].description + " -- " + data[i].publisher + " -- " + data[i].platform + " -- " + data[i].price + " -- " + data[i].reseller;
    }
    gameContainer.insertAdjacentHTML('beforeend', htmlString);
}
*/

function selectAnt(){
    var selectBox = document.getElementsByName("selectAntrenor")[0];
      var selectedValue = selectBox.options[selectBox.selectedIndex].value;


}

function addfields() {
  var selectBox = document.getElementsByName("selectlist")[0];
  var selectedValue = selectBox.options[selectBox.selectedIndex].value;
  var selectdiv = document.getElementById("CON");
  if (selectedValue == "inotator")
  {
    selectdiv.innerHTML = "";
    var formf = document.createElement("form");
    var inputf = document.createElement("input");
    var labelf = document.createElement("p");
    var ant_tabel = document.createElement("div");

    formf.setAttribute("action", "add/submitinot");

    labelf.innerHTML = "Nume: ";
    inputf.setAttribute("type", "text");
    inputf.setAttribute("name", "nume_inot");
    labelf.appendChild(inputf);
    formf.appendChild(labelf);

    labelf = document.createElement("p");
    inputf = document.createElement("input");
    labelf.innerHTML = "Prenume: ";
    inputf.setAttribute("type", "text");
    inputf.setAttribute("name", "prenume_inot");
    labelf.appendChild(inputf);
    formf.appendChild(labelf);

    labelf = document.createElement("p");
    inputf = document.createElement("input");
    labelf.innerHTML = "Categorie: ";
    inputf.setAttribute("type", "text");
    inputf.setAttribute("name", "categorie");
    labelf.appendChild(inputf);
    formf.appendChild(labelf);

    labelf = document.createElement("p");
    inputf = document.createElement("input");
    labelf.innerHTML = "Nastere: ";
    inputf.setAttribute("type", "date");
    inputf.setAttribute("max", "2011-01-02");
    inputf.setAttribute("name", "nastere");
    labelf.appendChild(inputf);
    formf.appendChild(labelf);

    labelf = document.createElement("p");
    inputf = document.createElement("input");
    labelf.innerHTML = "Antrenor: ";
    inputf.setAttribute("type", "text");
    inputf.setAttribute("name", "antrenor_nume");

    var selectAntrenor = document.createElement("select");
    var optionAntrenor = document.createElement("option");
    var optionDefault = document.createElement("option");
    optionDefault.setAttribute("value", "null");
    optionDefault.innerHTML = "Alege un antrenor:";
    optionAntrenor.setAttribute("th:each",'oneAntrenor : ${antrenori}');
    optionAntrenor.setAttribute("th:value", '${oneAntrenor.getId_ant()}');
    optionAntrenor.setAttribute("th:text", '${oneAntrenor.getNume_ant()}' );
    selectAntrenor.appendChild(optionDefault);
    selectAntrenor.appendChild(optionAntrenor);

    labelf.appendChild(inputf);
    labelf.appendChild(selectAntrenor);
    formf.appendChild(labelf);

    inputf = document.createElement("input");
    inputf.setAttribute("type", "submit");
    inputf.setAttribute("value", "Submit");
    formf.appendChild(inputf);
    selectdiv.appendChild(formf);
  }
  else if (selectedValue == "antrenor") {
      selectdiv.innerHTML = "";
      var formf = document.createElement("form");
      var inputf = document.createElement("input");
      var labelf = document.createElement("p");

      formf.setAttribute("action", "add/submitant");


      labelf.innerHTML = "Nume: ";
      inputf.setAttribute("type", "text");
      inputf.setAttribute("name", "nume_ant");
      labelf.appendChild(inputf);
      formf.appendChild(labelf);

      labelf = document.createElement("p");
      inputf = document.createElement("input");
      labelf.innerHTML = "Prenume: ";
      inputf.setAttribute("type", "text");
      inputf.setAttribute("name", "prenume_ant");
      labelf.appendChild(inputf);
      formf.appendChild(labelf);

      labelf = document.createElement("p");
      inputf = document.createElement("input");
      labelf.innerHTML = "Nastere: ";
      inputf.setAttribute("type", "date");
      inputf.setAttribute("max", "2000-01-02");
      inputf.setAttribute("name", "nastere");
      labelf.appendChild(inputf);
      formf.appendChild(labelf);

      inputf = document.createElement("input");
      inputf.setAttribute("type", "submit");
      inputf.setAttribute("value", "Submit");
      formf.appendChild(inputf);
      selectdiv.appendChild(formf);
    }
  else if (selectedValue == "concurs") {
      selectdiv.innerHTML = "";
      var formf = document.createElement("form");
      var inputf = document.createElement("input");
      var labelf = document.createElement("p");

      formf.setAttribute("action", "add/submitconc");


      labelf.innerHTML = "Denumire: ";
      inputf.setAttribute("type", "text");
      inputf.setAttribute("name", "denumire");
      labelf.appendChild(inputf);
      formf.appendChild(labelf);

      labelf = document.createElement("p");
      inputf = document.createElement("input");
      labelf.innerHTML = "Localitate: ";
      inputf.setAttribute("type", "text");
      inputf.setAttribute("name", "localitate");
      labelf.appendChild(inputf);
      formf.appendChild(labelf);

      labelf = document.createElement("p");
      inputf = document.createElement("input");
      labelf.innerHTML = "Data: ";
      inputf.setAttribute("type", "date");
      inputf.setAttribute("max", "2017-07-18");
      inputf.setAttribute("name", "nastere");
      labelf.appendChild(inputf);
      formf.appendChild(labelf);

      inputf = document.createElement("input");
      inputf.setAttribute("type", "submit");
      inputf.setAttribute("value", "Submit");
      formf.appendChild(inputf);
      selectdiv.appendChild(formf);
    }

}

function participSelect()
{
    $.ajax({
            type : "POST",
            url : "add",
            contentType: "application/json; charset=utf-8",
            dataType : "json",
            async: true,
            success : function(result) {
                var od = JSON.stringify(result) ;
                console.log(od);
            }
        });

}


function addInotator() {

    if (document.getElementById("numeInot").value == "") {
        alert("Please give a lastname!")
        return false;
    }

    if (document.getElementById("prenumeInot").value == "") {
            alert("Please give a firstname!")
            return false;
        }

    if (document.getElementById("categorieInot").value == "" || document.getElementById("categorieInot").value.length > 1) {
                alert("Please give a character between A-Z!")
                return false;
            }

    if (document.getElementById("selectAntrenor").value == "null") {
                alert("Please select a Coach!")
                return false;
            }

        $.ajax({
                type: "POST",
                url: "/add/addInotator",
                data: {
                    "nume_inot": document.getElementById("numeInot").value,
                    "prenume_inot": document.getElementById("prenumeInot").value,
                    "categorie": document.getElementById("categorieInot").value,
                    "nastere": document.getElementById("nastereInot").value,
                    "id_antrenor": document.getElementById("selectAntrenor").value
                },
                dataType: "json"
            }).done(function() {
                            alert("Inotator inserted!")
                            $("#numeInot").val("");
                            $("#prenumeInot").val("");
                            $("#categorieInot").val("");
                            $("#nastereInot").val("");
                            $("#selectAntrenor").val("null");
                        }).fail(function() {
                            alert("Error");
                        });
}

function addAntrenor() {

    if (document.getElementById("numeAnt").value == "") {
        alert("Please give a lastname!")
        return false;
    }

    if (document.getElementById("prenumeAnt").value == "") {
            alert("Please give a firstname!")
            return false;
        }

        $.ajax({
                type: "POST",
                url: "/add/addAntrenor",
                data: {
                    "nume_ant": document.getElementById("numeAnt").value,
                    "prenume_ant": document.getElementById("prenumeAnt").value,
                    "nastere": document.getElementById("nastereAnt").value
                },
                dataType: "json"
            }).done(function() {
                            location.reload();
                            alert("Antrenor inserted!")
                            $("#numeAnt").val("");
                            $("#prenumeAnt").val("");
                            $("#nastereAnt").val("");
                        }).fail(function() {
                            alert("Error");
                        });
}

function addConcurs() {

    if (document.getElementById("denumireConc").value == "") {
        alert("Please give a name!")
        return false;
    }

    if (document.getElementById("localitateConc").value == "") {
            alert("Please give a location!")
            return false;
        }

        $.ajax({
                type: "POST",
                url: "/add/addConcurs",
                data: {
                    "denumire": document.getElementById("denumireConc").value,
                    "localitate": document.getElementById("localitateConc").value,
                    "data": document.getElementById("dataConc").value
                },
                dataType: "json"
            }).done(function() {
                            location.reload();
                            alert("Concurs inserted!")
                            $("#denumireConc").val("");
                            $("#localitateConc").val("");
                            $("#dataConc").val("");
                        }).fail(function() {
                            alert("Error");
                        });
}

function addParticip() {

    if (document.getElementById("selectConcParticip").value == "null") {
        alert("Please select a competition!")
        return false;
    }

    if (document.getElementById("selectParticip").value == "null") {
            alert("Please select a swimmer!")
            return false;
        }

        $.ajax({
                type: "POST",
                url: "/add/addParticip",
                data: {
                    "id_inot": document.getElementById("selectParticip").value,
                    "id_conc": document.getElementById("selectConcParticip").value
                },
                dataType: "json"
            }).done(function() {
                            location.reload();
                            alert("Participant inserted!")
                            $("#selectParticip").val("");
                            $("#selectConcParticip").val("");
                        }).fail(function() {
                            alert("Error");
                        });
}

function removeInot()
{


    if($('input[name=radioInot]:checked').val())
    {
        $.ajax({
                        type: "POST",
                        url: "/remove/removeInot",
                        data: {
                            "idInot": $('input[name=radioInot]:checked').val()
                        },
                        dataType: "json"
                    }).done(function() {
                                    location.reload();
                                    alert("Inotator removed!");
                                }).fail(function() {
                                    alert("Error");
                                });
    }
}

function removeAnt()
{
    if($('input[name=radioAnt]:checked').val())
        {
            $.ajax({
                            type: "POST",
                            url: "/remove/removeAnt",
                            data: {
                                "idAnt": $('input[name=radioAnt]:checked').val()
                            },
                            dataType: "json"
                        }).done(function() {
                                        location.reload();
                                        alert("Antrenor removed!");
                                    }).fail(function() {
                                        alert("Error");
                                    });
        }

}

function removeConc()
{
    if($('input[name=radioConc]:checked').val())
        {
            $.ajax({
                            type: "POST",
                            url: "/remove/removeConc",
                            data: {
                                "idConc": $('input[name=radioConc]:checked').val()
                            },
                            dataType: "json"
                        }).done(function() {
                                        location.reload();
                                        alert("Concurs removed!");
                                    }).fail(function() {
                                        alert("Error");
                                    });
        }

}

function updateInot()
{
    if($('input[name=radioInotEdit]:checked').val())
    {
        parent = $('input[name=radioInotEdit]:checked').parent().parent();
        idInotEdit = $('input[name=radioInotEdit]:checked').val();
        numeInotEdit = parent.children().eq(1).text();
        prenumeInotEdit = parent.children().eq(2).text();
        categorieInotEdit = parent.children().eq(3).text();
        nastereInotEdit = parent.children().eq(4).children().val();
        antrenorInotEdit = parent.children().eq(5).children().val();

        if(numeInotEdit == ""){
            alert("Nume field must not be empty!");
            return false;
        }

        if(prenumeInotEdit == ""){
                    alert("Prenume field must not be empty!");
                    return false;
        }

        if(categorieInotEdit.length > 1 || categorieInotEdit.length == 0){
            alert("Category must be a char between A-Z!");
            return false;
        }

        if(nastereInotEdit == ""){
                    alert("Nastere field must not be empty!");
                    return false;
        }

        $.ajax({
                                type: "POST",
                                url: "/update/updateInot",
                                data: {
                                    "idInot": idInotEdit,
                                    "numeInot": numeInotEdit,
                                    "prenumeInot": prenumeInotEdit,
                                    "categorieInot": categorieInotEdit,
                                    "nastereInot": nastereInotEdit,
                                    "idAnt": antrenorInotEdit
                                },
                                dataType: "json"
                            }).done(function() {
                                            location.reload();
                                            alert("Inotator updated!");
                                        }).fail(function() {
                                            alert("Error");
                                        });

    }
}

function updateAnt()
{
    if($('input[name=radioAntEdit]:checked').val())
    {
        parent = $('input[name=radioAntEdit]:checked').parent().parent();
        idAntEdit = $('input[name=radioAntEdit]:checked').val();
        numeAntEdit = parent.children().eq(1).text();
        prenumeAntEdit = parent.children().eq(2).text();
        nastereAntEdit = parent.children().eq(3).children().val();

        if(numeAntEdit == ""){
            alert("Nume field must not be empty!");
            return false;
        }

        if(prenumeAntEdit == ""){
                    alert("Prenume field must not be empty!");
                    return false;
        }

        if(nastereAntEdit == ""){
                    alert("Nastere field must not be empty!");
                    return false;
        }

        $.ajax({
                                type: "POST",
                                url: "/update/updateAnt",
                                data: {
                                    "idAnt": idAntEdit,
                                    "numeAnt": numeAntEdit,
                                    "prenumeAnt": prenumeAntEdit,
                                    "nastereAnt": nastereAntEdit
                                },
                                dataType: "json"
                            }).done(function() {
                                            location.reload();
                                            alert("Antrenor updated!");
                                        }).fail(function() {
                                            alert("Error");
                                        });

    }
}

function updateConc()
{
    if($('input[name=radioConcEdit]:checked').val())
    {
        parent = $('input[name=radioConcEdit]:checked').parent().parent();
        idConcEdit = $('input[name=radioConcEdit]:checked').val();
        denumireConcEdit = parent.children().eq(1).text();
        localitateConcEdit = parent.children().eq(2).text();
        dataConcEdit = parent.children().eq(3).children().val();

        if(denumireConcEdit == ""){
            alert("Denumire field must not be empty!");
            return false;
        }

        if(localitateConcEdit == ""){
                    alert("Localitate field must not be empty!");
                    return false;
        }

        if(dataConcEdit == ""){
                    alert("Data field must not be empty!");
                    return false;
        }

        $.ajax({
                                type: "POST",
                                url: "/update/updateConc",
                                data: {
                                    "idConc": idConcEdit,
                                    "denumireConc": denumireConcEdit,
                                    "localitateConc": localitateConcEdit,
                                    "dataConc": dataConcEdit
                                },
                                dataType: "json"
                            }).done(function() {
                                            location.reload();
                                            alert("Concurs updated!");
                                        }).fail(function() {
                                            alert("Error");
                                        });

    }
}


function updateParticip()
{
    if($('#selectInotEdit').val() == "null")
    {
        alert("Please select a swimmer");
        return false;
    }

    var array = [];

    $('input:checkbox').each(function(){
        if($(this).is(':checked'))
        {
            array.push($(this).val());
        }
    });

    var idInot = $('#selectInotEdit').val();

    $.ajax({
                                        type: "POST",
                                        url: "/update/updateParticip",
                                        data: {
                                            "idInot": idInot,
                                            newConcList: array
                                        },
                                        dataType: "json"
                                    }).done(function() {
                                                    location.reload();
                                                    alert("Participant updated!");
                                                }).fail(function() {
                                                    alert("Error");
                                                });


}