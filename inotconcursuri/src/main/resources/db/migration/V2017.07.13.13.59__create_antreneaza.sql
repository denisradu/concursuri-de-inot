CREATE TABLE antreneaza(
	id_inot INT,
    id_ant INT,
    PRIMARY KEY(id_inot),
    FOREIGN KEY(id_inot) REFERENCES inotatori(id_inot),
    FOREIGN KEY(id_ant) REFERENCES antrenori(id_ant)
);