INSERT INTO concursuri (denumire,localitate,data) VALUES ('Concursul international STAR 2017','Kranj',STR_TO_DATE('14-01-2018','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Flanders Speedo Cup','Antwerpen',STR_TO_DATE('21-06-2020','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Cupa Contratimp Chindia','Targoviste',STR_TO_DATE('18-02-2019','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Campionatele regionale de copii','Oradea',STR_TO_DATE('7-04-2018','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Cupa Hyperion','Brasov',STR_TO_DATE('29-04-2021','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Cupa Bistritei','Bacau',STR_TO_DATE('12-05-2020','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Cupa Parafa','Gyongyos',STR_TO_DATE('10-06-2018','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Cupa mondiala FINA Airweave','Moskova',STR_TO_DATE('02-08-2017','%d-%m-%Y'));
INSERT INTO concursuri (denumire,localitate,data) VALUES ('Cupa Somes','Beclean',STR_TO_DATE('14-10-2017','%d-%m-%Y'));
