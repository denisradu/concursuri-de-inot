CREATE TABLE participa(
	id_inot INT,
    id_conc INT,
    PRIMARY KEY(id_inot,id_conc),
    FOREIGN KEY(id_inot) REFERENCES inotatori(id_inot),
    FOREIGN KEY(id_conc) REFERENCES concursuri(id_conc)
);