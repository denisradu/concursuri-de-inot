package boot.repository;

import boot.model.Concursuri;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Mindit on 13-Jul-17.
 */
@Repository
public interface ConcursuriRepository extends JpaRepository<Concursuri, Integer> {
    @Query(value = "select * from concursuri where id_conc=?1",nativeQuery = true)
    public Concursuri getById(Integer id);
}
