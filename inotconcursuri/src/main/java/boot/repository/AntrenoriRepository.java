package boot.repository;

import boot.model.Antrenori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Mindit on 13-Jul-17.
 */
@Repository
public interface AntrenoriRepository extends JpaRepository<Antrenori, Integer> {
    @Query(value = "select * from antrenori where id_ant=?1", nativeQuery = true)
    public Antrenori getById_Inot(Integer id);

    @Query(value = "select id_ant from antrenori", nativeQuery = true)
    public List<Integer> getAllIds();
}
