package boot.repository;

import boot.model.Inotatori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Mindit on 13-Jul-17.
 */
@Repository
public interface InotatoriRepository extends JpaRepository<Inotatori, Integer> {
    @Query(value = "select * from inotatori where id_inot=?1",nativeQuery = true)
    public Inotatori getById(Integer id);


}
