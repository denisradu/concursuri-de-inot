package boot.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Mindit on 13-Jul-17.
 */
@Entity
@Table(name = "concursuri")
public class Concursuri {
    @Id
    @GeneratedValue
    @Column(name = "id_conc")
    private Integer id_conc;
    @Column(name = "denumire")
    private String denumire;
    @Column(name = "localitate")
    private String localitate;
    @Column(name = "data")
    private Date data;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "participa", joinColumns = {
            @JoinColumn(name = "id_conc", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "id_inot",
                    nullable = false, updatable = false) })
    private Set<Inotatori> inotatoriC = new HashSet<>(0);

    public Integer getId_conc() {
        return id_conc;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public String getLocalitate() {
        return localitate;
    }

    public void setLocalitate(String localitate) {
        this.localitate = localitate;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Concursuri(){}

    public Concursuri(String denumire, String localitate, Date data) {
        this.denumire = denumire;
        this.localitate = localitate;
        this.data = data;
    }

    public Concursuri(String denumire, String localitate, Date data, Set<Inotatori> inotatoriC) {
        this.denumire = denumire;
        this.localitate = localitate;
        this.data = data;
        this.inotatoriC = inotatoriC;
    }

    public Set<Inotatori> getInotatoriC() {
        return inotatoriC;
    }

    public void setInotatoriC(Set<Inotatori> inotatoriC) {
        this.inotatoriC = inotatoriC;
    }

    public String getStringData() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        int year = cal.get(Calendar.YEAR);
        String month = cal.get(Calendar.MONTH) + 1<10?"0"+(cal.get(Calendar.MONTH)+1):""+(cal.get(Calendar.MONTH) + 1);
        String day = cal.get(Calendar.DAY_OF_MONTH)<10?"0" +cal.get(Calendar.DAY_OF_MONTH):""+cal.get(Calendar.DAY_OF_MONTH);
        String htmlDate = "" + year + "-" + month + "-" + day;
        return htmlDate;
    }
}
