package boot.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Mindit on 13-Jul-17.
 */
@Entity
@Table(name = "antrenori")
public class Antrenori{
    @Id
    @GeneratedValue
    @Column(name = "id_ant")
    private int id_ant;
    @Column(name = "nume_ant")
    private String nume_ant;
    @Column(name = "prenume_ant")
    private String prenume_ant;
    @Column(name = "nastere")
    private Date nastere;

    @OneToMany(mappedBy = "antrenor", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private Set<Inotatori> inotatoriA = new HashSet<>(0);

    public Antrenori(){}

    public Antrenori(String nume_ant, String prenume_ant, Date nastere){
        this.nume_ant = nume_ant;
        this.prenume_ant = prenume_ant;
        this.nastere = nastere;
    }

    public int getId_ant() {
        return id_ant;
    }

    public String getNume_ant() {
        return nume_ant;
    }

    public void setNume_ant(String nume_ant) {
        this.nume_ant = nume_ant;
    }

    public String getPrenume_ant() {
        return prenume_ant;
    }

    public void setPrenume_ant(String prenume_ant) {
        this.prenume_ant = prenume_ant;
    }

    public Date getNastere() {
        return nastere;
    }

    public void setNastere(Date nastere) {
        this.nastere = nastere;
    }

    public Antrenori(String nume_ant, String prenume_ant, Date nastere, Set<Inotatori> inotatori){
        this.nume_ant = nume_ant;
        this.prenume_ant = prenume_ant;
        this.nastere = nastere;
        this.inotatoriA = inotatori;
    }


    public Set<Inotatori> getInotatori(){
        return this.inotatoriA;
    }

    public void setInotatori(Set<Inotatori> inotatori) {
        this.inotatoriA = inotatori;
    }

    public String getStringNastere() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(nastere);
        int year = cal.get(Calendar.YEAR);
        String month = cal.get(Calendar.MONTH) + 1<10?"0"+(cal.get(Calendar.MONTH)+1):""+(cal.get(Calendar.MONTH) + 1);
        String day = cal.get(Calendar.DAY_OF_MONTH)<10?"0" +cal.get(Calendar.DAY_OF_MONTH):""+cal.get(Calendar.DAY_OF_MONTH);
        String htmlDate = "" + year + "-" + month + "-" + day;
        return htmlDate;
    }
}
