package boot.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Mindit on 13-Jul-17.
 */
@Entity
@Table(name = "inotatori")
public class Inotatori {
    @Id
    @GeneratedValue
    @Column(name = "id_inot")
    private Integer id_inot;
    @Column(name = "nume_inot")
    private String nume_inot;
    @Column(name = "prenume_inot")
    private String prenume_inot;
    @Column(name = "categorie")
    private Character categorie;
    @Column(name = "nastere")
    private Date nastere;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_antrenor")
    @JsonBackReference
    private Antrenori antrenor;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "inotatoriC")
    private Set<Concursuri> concursuri = new HashSet<>(0);

    public Integer getId_inot() {
        return id_inot;
    }

    public String getNume_inot() {
        return nume_inot;
    }

    public void setNume_inot(String nume_inot) {
        this.nume_inot = nume_inot;
    }

    public String getPrenume_inot() {
        return prenume_inot;
    }

    public void setPrenume_inot(String prenume_inot) {
        this.prenume_inot = prenume_inot;
    }

    public Character getCategorie() {
        return categorie;
    }

    public void setCategorie(Character categorie) {
        this.categorie = categorie;
    }

    public Date getNastere() {
        return nastere;
    }

    public String getStringNastere() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(nastere);
        int year = cal.get(Calendar.YEAR);
        String month = cal.get(Calendar.MONTH) + 1<10?"0"+(cal.get(Calendar.MONTH)+1):""+(cal.get(Calendar.MONTH) + 1);
        String day = cal.get(Calendar.DAY_OF_MONTH)<10?"0" +cal.get(Calendar.DAY_OF_MONTH):""+cal.get(Calendar.DAY_OF_MONTH);
        String htmlDate = "" + year + "-" + month + "-" + day;
        return htmlDate;
    }

    public void setNastere(Date nastere) {
        this.nastere = nastere;
    }


    public Inotatori(){}

    public Inotatori(String nume_inot, String prenume_inot, Character categorie, Date nastere){
        this.nume_inot = nume_inot;
        this.prenume_inot = prenume_inot;
        this.categorie = categorie;
        this.nastere = nastere;
    }

    public Inotatori(String nume_inot, String prenume_inot, Character categorie, Date nastere, Antrenori antrenor) {
        this.nume_inot = nume_inot;
        this.prenume_inot = prenume_inot;
        this.categorie = categorie;
        this.nastere = nastere;
        this.antrenor = antrenor;
    }

    public Antrenori getAntrenor(){
        return this.antrenor;
    }

    public void setAntrenori(Antrenori antrenor){
        this.antrenor = antrenor;
    }

    public Set<Concursuri> getConcursuri() {
        return concursuri;
    }

    public String getConcursuriDenumire(){
        String listConcursuri="";
        for(Concursuri conc: concursuri)
            listConcursuri+=conc.getDenumire()+", \n\r";
        return listConcursuri;
    }

    public List<String> getListConcursuriDenumire() {
        List<String> list = new ArrayList<>();
        for (Concursuri conc : concursuri)
            list.add(conc.getDenumire());
        return list;
    }

    public List<Integer> getListConcursuriId() {
        List<Integer> list = new ArrayList<>();
        for (Concursuri conc : concursuri)
            list.add(conc.getId_conc());
        return list;
    }

    public Integer getAntrenorId()
    {
        return antrenor.getId_ant();
    }

    public String getAntrenorNumePrenume(){
        String nume = antrenor.getNume_ant() + " " + antrenor.getPrenume_ant();
        return nume;
    }

    public void setConcursuri(Set<Concursuri> concursuri) {
        this.concursuri = concursuri;
    }

    public List<Integer> getConcursuriId(){
        List<Integer> listConc = new ArrayList<>();
        for(Concursuri conc: concursuri)
            listConc.add(conc.getId_conc());
        return listConc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inotatori inotatori = (Inotatori) o;

        return id_inot.equals(inotatori.id_inot);
    }

    @Override
    public int hashCode() {
        return id_inot.hashCode();
    }
}
