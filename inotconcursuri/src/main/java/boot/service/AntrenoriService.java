package boot.service;

import boot.model.Antrenori;

import java.util.List;

/**
 * Created by Mindit on 14-Jul-17.
 */
public interface AntrenoriService {

    public List<Antrenori> getAllAntrenori();

    public Antrenori getAntrenorById_Inot(Integer id);

    public void saveAndFlush(Antrenori ant);

    public Antrenori getAntrenorById(Integer id);

    public void removeAntrenor(Integer id);
}
