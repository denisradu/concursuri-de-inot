package boot.service;

import boot.model.Concursuri;

import java.util.List;

/**
 * Created by Mindit on 14-Jul-17.
 */
public interface ConcursuriService {
    public List<Concursuri> getAllConcursuri();
    public void saveAndFlush(Concursuri conc);
    public Concursuri getConcursById(Integer id);
    public void removeConcurs(Integer id);
}
