package boot.service;

import boot.model.Concursuri;
import boot.model.Inotatori;
import boot.repository.ConcursuriRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Mindit on 14-Jul-17.
 */
@Service
public class ConcursuriServiceImpl implements ConcursuriService{
    @Autowired
    ConcursuriRepository concursuriRepository;


    public List<Concursuri> getAllConcursuri()
    {
        return concursuriRepository.findAll();
    }

    public void saveAndFlush(Concursuri conc)
    {
        concursuriRepository.saveAndFlush(conc);
    }

    public Concursuri getConcursById(Integer id)
    {
        return concursuriRepository.getById(id);
    }

    public void removeConcurs(Integer id){
        try{

            concursuriRepository.delete(id);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
