package boot.service;

import boot.model.Inotatori;

import java.util.List;

/**
 * Created by Mindit on 14-Jul-17.
 */
public interface InotatoriService {
    public List<Inotatori> getAllInotatori();
    public void saveAndFlush(Inotatori inot);
    public Inotatori getInotatorById(Integer id);
    public void removeInotator(Integer id);
}
