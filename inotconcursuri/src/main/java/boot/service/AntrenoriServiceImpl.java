package boot.service;

import boot.model.Antrenori;
import boot.model.Inotatori;
import boot.repository.AntrenoriRepository;
import boot.repository.InotatoriRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by Mindit on 14-Jul-17.
 */
@Service
public class AntrenoriServiceImpl implements AntrenoriService{
    @Autowired
    AntrenoriRepository antrenoriRepository;

    @Autowired
    InotatoriRepository inotatoriRepository;

    @Autowired
    InotatoriService inotatoriService;

    public List<Antrenori> getAllAntrenori()
    {
        List<Antrenori> ants = antrenoriRepository.findAll();
        return ants;
    }

    public Antrenori getAntrenorById_Inot(Integer id)
    {
        return antrenoriRepository.getById_Inot(id);
    }

    public void saveAndFlush(Antrenori ant)
    {
        antrenoriRepository.saveAndFlush(ant);
    }

    public Antrenori getAntrenorById(Integer id)
    {
        return antrenoriRepository.getById_Inot(id);
    }

    public void removeAntrenor(Integer id){
        try{
            List<Inotatori> listInot = inotatoriRepository.findAll();
            for(Inotatori inot : listInot)
                if(inot.getAntrenor().getId_ant() == id)
                {
                    inotatoriService.removeInotator(inot.getId_inot());
                }
            antrenoriRepository.delete(id);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
