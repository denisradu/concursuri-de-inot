package boot.service;

import boot.model.Antrenori;
import boot.model.Concursuri;
import boot.model.Inotatori;
import boot.repository.ConcursuriRepository;
import boot.repository.InotatoriRepository;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Mindit on 14-Jul-17.
 */
@Service
public class InotatoriServiceImpl implements InotatoriService{
    @Autowired
    InotatoriRepository inotatoriRepository;

    @Autowired
    ConcursuriRepository concursuriRepository;


    public List<Inotatori> getAllInotatori()
    {
        return inotatoriRepository.findAll();
    }

    public void saveAndFlush(Inotatori inot)
    {
        inotatoriRepository.saveAndFlush(inot);
    }

    public Inotatori getInotatorById(Integer id)
    {
        return inotatoriRepository.getById(id);
    }

    public void removeInotator(Integer id)
    {
        try{
            Inotatori inotator = inotatoriRepository.getById(id);
            //inotator.setConcursuri(null);
            //inotatoriRepository.save(inotator);
            Set<Concursuri> listConc = inotator.getConcursuri();
            for(Concursuri conc: listConc)
            {
                Set<Inotatori> listInot = conc.getInotatoriC();
                listInot.remove(inotator);
                concursuriRepository.saveAndFlush(conc);
            }
            inotatoriRepository.delete(id);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
