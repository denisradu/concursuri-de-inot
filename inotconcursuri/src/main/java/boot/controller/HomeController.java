package boot.controller;

import boot.model.Antrenori;
import boot.model.Inotatori;
import boot.service.AntrenoriService;
import boot.service.ConcursuriService;
import boot.service.InotatoriService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mindit on 14-Jul-17.
 */
@Controller
public class HomeController {
    @Autowired
    AntrenoriService antrenoriService;
    @Autowired
    ConcursuriService concursuriService;
    @Autowired
    InotatoriService inotatoriService;

    @RequestMapping(value = "/")
    public String loadHome()
    {
        return "index";
    }

    @RequestMapping(value = "/add", method = {RequestMethod.GET})
    public String loadAdd(Model model)
    {
        model.addAttribute("inotatori", inotatoriService.getAllInotatori());
        model.addAttribute("antrenori", antrenoriService.getAllAntrenori());
        model.addAttribute("concursuri", concursuriService.getAllConcursuri());
        return "add";
    }


    @RequestMapping(value = "/update")
    public String loadUpdate(Model model)
    {
        model.addAttribute("inotatori", inotatoriService.getAllInotatori());
        model.addAttribute("antrenori", antrenoriService.getAllAntrenori());
        model.addAttribute("concursuri", concursuriService.getAllConcursuri());
        return "update";
    }

    @RequestMapping(value = "/remove")
    public String loadRemove(Model model)
    {
        model.addAttribute("inotatori", inotatoriService.getAllInotatori());
        model.addAttribute("antrenori", antrenoriService.getAllAntrenori());
        model.addAttribute("concursuri", concursuriService.getAllConcursuri());
        return "remove";
    }

    @RequestMapping(value = "/show", method = {RequestMethod.GET})
    public String loadShow(Model model)
    {
        model.addAttribute("inotatori", inotatoriService.getAllInotatori());
        model.addAttribute("antrenori", antrenoriService.getAllAntrenori());
        model.addAttribute("concursuri", concursuriService.getAllConcursuri());
        return "show";
    }

    @RequestMapping(value = "/test", method = {RequestMethod.GET})
    public String loadTestPage(Model model)
    {
        model.addAttribute("inotatori", inotatoriService.getAllInotatori());
        model.addAttribute("antrenori", antrenoriService.getAllAntrenori());
        model.addAttribute("concursuri", concursuriService.getAllConcursuri());
        return "test";
    }
}
