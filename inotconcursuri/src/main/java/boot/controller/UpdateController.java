package boot.controller;

import boot.model.Antrenori;
import boot.model.Concursuri;
import boot.model.Inotatori;
import boot.service.AntrenoriService;
import boot.service.ConcursuriService;
import boot.service.InotatoriService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Mindit on 19-Jul-17.
 */
@Controller
@RequestMapping("/update")
public class UpdateController {
    @Autowired
    AntrenoriService antrenoriService;

    @Autowired
    InotatoriService inotatoriService;

    @Autowired
    ConcursuriService concursuriService;

    public final static Log LOGGER = LogFactory.getLog(UpdateController.class);

    @RequestMapping(value = "/updateInot", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateInot(@ModelAttribute("idInot") String idInot,
                          @ModelAttribute("numeInot") String numeInot,
                          @ModelAttribute("prenumeInot") String prenumeInot,
                          @ModelAttribute("categorieInot") String categorieInot,
                          @ModelAttribute("nastereInot") String nastereInot,
                          @ModelAttribute("idAnt") String idAnt) {

        Inotatori inotator = inotatoriService.getInotatorById(Integer.valueOf(idInot));
        if (inotator != null) {
            Antrenori antrenor = antrenoriService.getAntrenorById_Inot(Integer.valueOf(idAnt));
            if (antrenor != null) {
                inotator.setNume_inot(numeInot);
                inotator.setPrenume_inot(prenumeInot);
                inotator.setCategorie(categorieInot.charAt(0));
                try {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date date;
                    try {
                        date = format.parse(nastereInot);
                        inotator.setNastere(date);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                    return HttpServletResponse.SC_FORBIDDEN;
                }

                inotator.setAntrenori(antrenor);
                inotatoriService.saveAndFlush(inotator);
            } else return HttpServletResponse.SC_FORBIDDEN;

        } else return HttpServletResponse.SC_FORBIDDEN;
        return HttpServletResponse.SC_OK;
    }

    @RequestMapping(value = "/updateAnt", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateAnt(@ModelAttribute("idAnt") String idAnt,
                         @ModelAttribute("numeAnt") String numeAnt,
                         @ModelAttribute("prenumeAnt") String prenumeAnt,
                         @ModelAttribute("nastereAnt") String nastereAnt)
    {

        Antrenori antrenor = antrenoriService.getAntrenorById(Integer.valueOf(idAnt));
        if(antrenor != null)
        {
            antrenor.setNume_ant(numeAnt);
            antrenor.setPrenume_ant(prenumeAnt);
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date;
                try {
                    date = format.parse(nastereAnt);
                    antrenor.setNastere(date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                return HttpServletResponse.SC_FORBIDDEN;
            }
            antrenoriService.saveAndFlush(antrenor);
        }
        else return HttpServletResponse.SC_FORBIDDEN;
        return HttpServletResponse.SC_OK;
    }

    @RequestMapping(value = "/updateConc", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateConc(@ModelAttribute("idConc") String idConc,
                         @ModelAttribute("denumireConc") String denumireConc,
                         @ModelAttribute("localitateConc") String localitateConc,
                         @ModelAttribute("dataConc") String dataConc)
    {

        Concursuri concurs = concursuriService.getConcursById(Integer.valueOf(idConc));
        if(concurs != null)
        {
            concurs.setDenumire(denumireConc);
            concurs.setLocalitate(localitateConc);
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date;
                try {
                    date = format.parse(dataConc);
                    concurs.setData(date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                return HttpServletResponse.SC_FORBIDDEN;
            }
            concursuriService.saveAndFlush(concurs);
        }
        else return HttpServletResponse.SC_FORBIDDEN;
        return HttpServletResponse.SC_OK;
    }


    @RequestMapping(value = "/getConcForInot", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Integer> getConcForInot(@ModelAttribute("idInot") String idInot, Model model)
    {
        Inotatori inotator = inotatoriService.getInotatorById(Integer.valueOf(idInot));

        return inotator.getConcursuriId();
    }

    @RequestMapping(value = "/updateParticip", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateParticip(@ModelAttribute("idInot") String idInot, HttpServletRequest request)
    {
        Inotatori inot = inotatoriService.getInotatorById(Integer.valueOf(idInot));
        String[] newList = request.getParameterValues("newConcList[]");
        if(inot != null)
        {
            try {
                Set<Concursuri> concList = new HashSet<>(0);
                List<Concursuri> allConcList = concursuriService.getAllConcursuri();
                for(Concursuri conc: allConcList)
                {
                    Set<Inotatori> inotList = conc.getInotatoriC();
                    inotList.remove(inot);
                    conc.setInotatoriC(inotList);
                    concursuriService.saveAndFlush(conc);
                }
                for(String idConc: newList) {
                    Concursuri conc = concursuriService.getConcursById(Integer.valueOf(idConc));
                    Set<Inotatori> listInot = conc.getInotatoriC();
                    listInot.add(inot);
                    conc.setInotatoriC(listInot);
                    concursuriService.saveAndFlush(conc);
                    concList.add(conc);
                }

                inot.setConcursuri(concList);
                inotatoriService.saveAndFlush(inot);

            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                return HttpServletResponse.SC_FORBIDDEN;
            }
        }
        else{
            return HttpServletResponse.SC_FORBIDDEN;
        }
        return HttpServletResponse.SC_OK;

    }
}
