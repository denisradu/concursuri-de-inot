package boot.controller;

import boot.model.Antrenori;
import boot.model.Concursuri;
import boot.model.Inotatori;
import boot.service.AntrenoriService;
import boot.service.ConcursuriService;
import boot.service.InotatoriService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Mindit on 19-Jul-17.
 */

@Controller
@RequestMapping("/remove")
public class RemoveController {

    @Autowired
    AntrenoriService antrenoriService;

    @Autowired
    InotatoriService inotatoriService;

    @Autowired
    ConcursuriService concursuriService;

    public final static Log LOGGER = LogFactory.getLog(RemoveController.class);

    @RequestMapping(value = "/removeInot", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int removeInot(@ModelAttribute("idInot") String idInot){
        Inotatori inotator = inotatoriService.getInotatorById(Integer.valueOf(idInot));
        if(inotator != null)
        {
            inotatoriService.removeInotator(Integer.valueOf(idInot));
            return HttpServletResponse.SC_OK;
        }
        else return HttpServletResponse.SC_FORBIDDEN;

    }

    @RequestMapping(value = "/removeAnt", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int removeAnt(@ModelAttribute("idAnt") String idAnt){
        Antrenori antrenor = antrenoriService.getAntrenorById(Integer.valueOf(idAnt));
        if(antrenor != null)
        {
            antrenoriService.removeAntrenor(Integer.valueOf(idAnt));
            return HttpServletResponse.SC_OK;
        }
        else return HttpServletResponse.SC_FORBIDDEN;

    }

    @RequestMapping(value = "/removeConc", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int removeConc(@ModelAttribute("idConc") String idConc){
        Concursuri concurs = concursuriService.getConcursById(Integer.valueOf(idConc));
        if(concurs != null)
        {
            concursuriService.removeConcurs(Integer.valueOf(idConc));
            return HttpServletResponse.SC_OK;
        }
        else return HttpServletResponse.SC_FORBIDDEN;

    }
}
