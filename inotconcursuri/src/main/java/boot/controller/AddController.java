package boot.controller;

import boot.model.Antrenori;
import boot.model.Concursuri;
import boot.model.Inotatori;
import boot.service.AntrenoriService;
import boot.service.ConcursuriService;
import boot.service.InotatoriService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Created by Mindit on 18-Jul-17.
 */
@Controller
@RequestMapping("/add")
public class AddController {

    @Autowired
    AntrenoriService antrenoriService;

    @Autowired
    InotatoriService inotatoriService;

    @Autowired
    ConcursuriService concursuriService;

    public final static Log LOGGER = LogFactory.getLog(AddController.class);


    /*@RequestMapping(value = "/submit", method = {RequestMethod.POST}, produces = "application/json")

    public String add(HttpServletRequest request)
    {
        if(request.getParameter("addInotator")!=null)
        {
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = format.parse(request.getParameter("nastereInot"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Inotatori inotator = new Inotatori(request.getParameter("numeInot"),
                    request.getParameter("prenumeInot"),
                    request.getParameter("categorieInot").charAt(0),
                    date,
                    Integer.valueOf(request.getParameter("selectAntrenor")));
            System.out.println(inotator.getNume_inot() + " -- " + inotator.getPrenume_inot()
                    + " -- " + inotator.getCategorie() + " -- " + inotator.getNastere() + " -- " + inotator.getId_antrenor());
        }
        return "redirect:submit";
    }*/

    @RequestMapping(value = "/addInotator", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int addInot(
            @ModelAttribute("nume_inot") String numeInot,
            @ModelAttribute("prenume_inot") String prenumeInot,
            @ModelAttribute("categorie") String categorie,
            @ModelAttribute("nastere") String nastere,
            @ModelAttribute("id_antrenor") String idAntrenor,
            Model model) {

        Antrenori antrenor = antrenoriService.getAntrenorById_Inot(Integer.valueOf(idAntrenor));
        if (antrenor != null) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date;
                try {
                    date = format.parse(nastere);
                    Inotatori inotator = new Inotatori(numeInot, prenumeInot, categorie.charAt(0), date, antrenor);
                    inotatoriService.saveAndFlush(inotator);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                model.addAttribute("errorMessage", e.getMessage());
                return HttpServletResponse.SC_FORBIDDEN;
            }


        }
        else return HttpServletResponse.SC_OK;
       return HttpServletResponse.SC_OK;
    }

    @RequestMapping(value = "/addAntrenor", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int addAnt(
            @ModelAttribute("nume_ant") String numeAnt,
            @ModelAttribute("prenume_ant") String prenumeAnt,
            @ModelAttribute("nastere") String nastere,
            Model model) {


            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date;
                try {
                    date = format.parse(nastere);
                    Antrenori antrenor = new Antrenori(numeAnt, prenumeAnt, date);
                    antrenoriService.saveAndFlush(antrenor);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                model.addAttribute("errorMessage", e.getMessage());
                return HttpServletResponse.SC_FORBIDDEN;
            }
        return HttpServletResponse.SC_OK;
    }

    @RequestMapping(value = "/addConcurs", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int addConc(
            @ModelAttribute("denumire") String denumire,
            @ModelAttribute("localitate") String localitate,
            @ModelAttribute("data") String data,
            Model model) {


        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date;
            try {
                date = format.parse(data);
                Concursuri conc = new Concursuri(denumire, localitate, date);
                concursuriService.saveAndFlush(conc);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            model.addAttribute("errorMessage", e.getMessage());
            return HttpServletResponse.SC_FORBIDDEN;
        }
        return HttpServletResponse.SC_OK;
    }

    @RequestMapping(value = "/addParticip", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int addParticip(
            @ModelAttribute("id_inot") String idInot,
            @ModelAttribute("id_conc") String idConc,
            Model model) {

        Inotatori inot = inotatoriService.getInotatorById(Integer.valueOf(idInot));
        Concursuri conc = concursuriService.getConcursById(Integer.valueOf(idConc));



        if (inot != null && conc != null)
        {
            try {
                Set<Concursuri> listConc = inot.getConcursuri();
                Set<Inotatori> listInot = conc.getInotatoriC();
                listConc.add(conc);
                listInot.add(inot);
                inot.setConcursuri(listConc);
                conc.setInotatoriC(listInot);
                inotatoriService.saveAndFlush(inot);
                concursuriService.saveAndFlush(conc);

            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                model.addAttribute("errorMessage", e.getMessage());
                return HttpServletResponse.SC_FORBIDDEN;
            }

        }
        return HttpServletResponse.SC_OK;
    }
}

