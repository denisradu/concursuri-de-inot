CREATE TABLE inotatori(
	id_inot INT auto_increment,
    nume_inot VARCHAR(100) NOT NULL,
    prenume_inot VARCHAR(255) NOT NULL,
    categorie CHAR NOT NULL,
    nastere DATE,
    PRIMARY KEY(id_inot)
);